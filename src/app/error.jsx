"use client";

import React from "react";

export default function ErrorPage({ error }) {
  return (
    <p>{error.message}</p>
  )
}
