// 👉 1. Don't use 'use client';

import React from 'react';
import HOCList from '@/components/HOCList';

export default async function TestServerPage({ searchParams }) {
  // initial query params with default value
  // to prevent 'null' or 'undefined' value
  const { page = 1, q = '' } = searchParams;
  
  // 👉 2. Fetching data on api depends on query params
  const fetchData = async () => {
    try {
      const response = await fetch(`http://192.168.101.186:4037/api/v1/question?page=${page}&q=${q}&sort=createdAt&sortType=descsc&testType[]=kuis`);

      // 👉 3. when error occurs error.jsx will handle it
      // when failed ❌
      if (!response.ok) throw new Error('response is not okay');

      // when success ✅
      return await response.json();
    } catch(error) {
      console.error(error);
      throw new Error(error);
    }
  };

  // store response into var
  const questionList = await fetchData();

  // 👉 4. send response from API to client component
  return <HOCList data={questionList} search={q} />;
}
