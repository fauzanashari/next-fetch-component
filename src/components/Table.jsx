"use client";

import React, { useEffect } from "react";
import { useRouter, usePathname, useSearchParams } from "next/navigation";

// 👉 9. In props above we receive props 'data' was passed in step #8,
//       'children' and 'onLoad' was passed by 'ListtableView' @sisfo-remake/resfoui
export default function Table({
  children,
  onLoad,
  data,
  perPage,
  page,
  total,
  search,
}) {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();

  const columns = [
    {
      name: 'question',
      selector: row => row.question
    },
    {
      name: 'subject',
      selector: row => row.subject.name
    }
  ];

  useEffect(() => {
    // 👉 10. setup table with dynamic data
    onLoad({
      data,
      columns,
      currentPage: page,
      pagination: true,
      paginationServe: true,
      paginationPerPage: perPage,
      paginationTotalRows: total,
      paginationAlign: "end",
      onChangePage: (page) => {
        // update to '?page=<page>'
        const params = new URLSearchParams(searchParams.toString());
        params.set('page', page);
        router.push(`${pathname}?${params.toString()}`);
      },
      withSearch: true,
      searchOptions: {
        placeholder: 'cari question',
        value: search,
        onSearch: (terms) => {
          // update to '?q=<keyword>'
          // TODO: fix logic when keyword is setted page must be restarted
          // to '1'
          const params = new URLSearchParams(searchParams.toString());
          params.set('q', terms);
          router.push(`${pathname}?${params.toString()}`);
        }
      }
    })
    // TODO: fix eslint error
  }, [data]);
  
  // 👉 11. render table through props 'children'
  return children;
}