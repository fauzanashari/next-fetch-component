// 👉 5. use 'use client';
"use client";

import React, { useMemo } from "react";
import { ListTableView } from "@sisfo-remake/resfoui";
import Table from "./Table"

// 👉 6. get response from server component through props
export default function MyPage({ data: response, search }) {
  const { page, perPage, total, totalPages, data } = response;

  // 👉 7. use 'useMemo' to update when 'data' is change
  //       and pass props 'data'
  const HOCTableFn = useMemo(() => (
    // ----------- 👇 pass 'Component' instead of 'Element'
    // ----------- 👇 in order to pass data
    ListTableView(<Table data={data} perPage={perPage} page={page} total={total} totalPages={totalPages} search={search} />, {
      titlePage: "Just for test",
      breadcrumbsRoute: [
        { label: "just for test 😀", url: "/" }
      ]
    })
  ), [data, perPage, page, total, totalPages, search]);

  // 👉 8. 'ListTableView' return function, so we should
  //        invoke it
  return HOCTableFn();
};
